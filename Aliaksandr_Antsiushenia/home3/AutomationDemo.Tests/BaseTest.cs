﻿using NUnit.Framework;
using AutomationTask3.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTask3.Tests
{
    class BaseTest
    {
        private const string baseUrl = "http://www.yandex.by";

        [SetUp]
        public void StartBrowser() => Browser.Instance.Start().OpenAt(baseUrl);

        [TearDown]
        public void CloseBrowser() => Browser.Instance.Close();

        public void Pause(int i) => Browser.Pause(i);
    }
}
