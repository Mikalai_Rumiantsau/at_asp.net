﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTask3.Models
{
    public class MailFactory
    {
        private const string defaultUserEmail = "mail@yandex.com";
        private const string defaultMailTitle = "Title";
        private const string defaultMailText = "SomeText+";

        private static readonly string RANDOM_TITLE = defaultMailTitle + new Random().Next();
        private static readonly string RANDOM_TEXT = defaultMailText + new Random().Next();

        public static Mail DefaultMail
        {
            get { return new Mail(defaultUserEmail, defaultMailTitle, defaultMailText); }
        }

        public static Mail RandomMail
        {
            get { return new Mail(defaultUserEmail, RANDOM_TITLE, RANDOM_TEXT); }
        }        
    }
}
