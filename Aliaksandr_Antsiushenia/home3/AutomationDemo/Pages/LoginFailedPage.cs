﻿using AutomationTask3.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTask3.Pages
{
    class LoginFailedPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By errorMessageByCss = By.CssSelector(".error-msg");
        internal IWebElement ErrorMessage { get { return browser.FindElement(errorMessageByCss); } }
    }
}
