﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services;
using AutomationDemo.Models;
using NUnit.Framework;

namespace AutomationDemo.Tests
{
    [TestFixture]
    class SendingMessageTest : BaseTest
    {
        SendingMessageService sendingMailService = new SendingMessageService();
        LoginService loginService = new LoginService();

        [Test]
        public void SendingMessageTest_SendingCorrectMessage_CheckingInInboxAndSentMail()
        {
            loginService.LoginToMailBox(UserAccountFactory.DefaultAccount);
            var currentMailMessage = MailMessageFactory.SampleMessageToDefaultAccount;
            sendingMailService.SendMail(currentMailMessage);
            Assert.IsTrue(sendingMailService.IsFoundSentLetter(currentMailMessage));
        }

    }
}