﻿using NUnit.Framework;
using AutomationDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Models;

namespace AutomationDemo.Tests
{
    [TestFixture]
    class LoginTest : BaseTest
    {
        LoginService loginService = new LoginService();

        private const string expected_error_message_in_case_of_account_not_exist = "Нет аккаунта с таким логином.";
        private const string expected_error_message_in_case_of_wrong_password = "Неправильный логин или пароль.";

        [Test]
        public void LoginTest_LoginWithExistingAccount_CorrectSuccessLabelPresent()
        {
            loginService.LoginToMailBox(UserAccountFactory.DefaultAccount);
            string actualUserNameLabel = loginService.RetrieveUserNameLabelOnSuccessLogin();
            Assert.AreEqual(actualUserNameLabel,UserAccountFactory.DefaultAccount.Email);
        }

        [Test]
        public void LoginTest_LoginWithNonExistingAccount_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(UserAccountFactory.NonExistedAccount);
            string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
            Assert.AreEqual(actualErrorMessage, expected_error_message_in_case_of_account_not_exist);
        }

        [Test]
        public void LoginTest_LoginWithAccountWithWrongPaswd_CorrectErrorMessagePresent()
        {
            loginService.LoginToMailBox(UserAccountFactory.AccountWithWrongPassword);
            string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
            Assert.AreEqual(actualErrorMessage, expected_error_message_in_case_of_wrong_password);
        }
    }
}
