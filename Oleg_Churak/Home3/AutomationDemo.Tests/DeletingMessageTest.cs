﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services;
using AutomationDemo.Models;
using NUnit.Framework;

namespace AutomationDemo.Tests
{
    class DeletingMessageTest : BaseTest
    {

        SendingMessageService sendingMailService = new SendingMessageService();
        LoginService loginService = new LoginService();
        MovementToTrashMessageService movementToTrashMessageService =
            new MovementToTrashMessageService();
        DeletingMessageService deletingMessageService = new DeletingMessageService();

        [Test]
        public void DeletingMessageTest_CheckingOfDeletingInTrash()
        {
            loginService.LoginToMailBox(UserAccountFactory.DefaultAccount);
            var currentMailMessage = MailMessageFactory.SampleMessageToDefaultAccount;
            sendingMailService.SendMail(currentMailMessage);
            movementToTrashMessageService.MoveMessageToTrash(currentMailMessage);
            deletingMessageService.DeleteMessege(currentMailMessage);

            Assert.That(deletingMessageService.IsNotFoundDeletedLetter(currentMailMessage));
        }           
    }
}