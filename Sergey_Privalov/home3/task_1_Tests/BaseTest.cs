﻿using NUnit.Framework;
using task_1.Framework;

namespace task_1_Tests
{
   public class BaseTest
    {
        private const string baseUrl = "http://www.yandex.by";

        [SetUp]
        public void StartBrowser() => Browser.Instance.Start().OpenAt(baseUrl);

        [TearDown]
        public void CloseBrowser() => Browser.Instance.Close();
    }
}
