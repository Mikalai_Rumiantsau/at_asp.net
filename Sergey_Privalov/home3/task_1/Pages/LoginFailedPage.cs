﻿using OpenQA.Selenium;
using task_1.Framework;

namespace task_1.Pages
{
    class LoginFailedPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By errorMessageByCss = By.CssSelector(".error-msg");

        internal IWebElement ErrorMessage
        {
            get
            {
                return browser.FindElement(errorMessageByCss);
            }
        }
    }
}
