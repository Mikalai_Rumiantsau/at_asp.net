﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using task_1.Framework;

namespace task_1.Pages
{
    public class MailFailedPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By errorMessageByCss = By.CssSelector("div[data-key*=ield-to-error]");

        internal IWebElement ErrorMessage
        {
            get
            {
                return browser.FindElement(errorMessageByCss);
            }
        }
        
    }
}
