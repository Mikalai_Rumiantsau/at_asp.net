﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Yandex.Models
{
    public class MessageModel
    {
        public string Recepient { get; set; }
        public string MessageSubject { get; set; }
        public string MessageText { get; set; }

        public MessageModel(string recepient, string messageSubject, string messageText)
        {
            Recepient = recepient;
            MessageSubject = messageSubject;
            MessageText = messageText;
        }
    }
}
