﻿using AT_Yandex.Models;
using AT_Yandex.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AT_Yandex.Services
{
    public class MessageService
    {
        private MailBoxHomePage mailBoxHomePage = new MailBoxHomePage();
        private MailBoxNewMessagePage mailBoxNewMessagePage = new MailBoxNewMessagePage();

        public void SendNewMessage(MessageModel message)
        {
            mailBoxHomePage.ComposeButton.Click();
            Thread.Sleep(1000);
            mailBoxNewMessagePage.MessageRecepientInput.SendKeys(message.Recepient);
            mailBoxNewMessagePage.MessageSubjectInput.SendKeys(message.MessageSubject);
            mailBoxNewMessagePage.MessageTextAreaInput.SendKeys(message.MessageText);
            mailBoxNewMessagePage.SendButton.Click();
            Thread.Sleep(1000);
        }

        public bool IsMessageInInbox(MessageModel message)
        {
            mailBoxHomePage.InboxButton.Click();
            Thread.Sleep(1000);
            mailBoxHomePage.CheckMailButton.Click();
            Thread.Sleep(1000);

            return mailBoxHomePage.IsMessageExists(message.MessageSubject);
        }

        public bool IsMessageInSent(MessageModel message)
        {
            mailBoxHomePage.SentButton.Click();
            Thread.Sleep(1000);
            mailBoxHomePage.CheckMailButton.Click();
            Thread.Sleep(1000);

            return mailBoxHomePage.IsMessageExists(message.MessageSubject);
        }

        public bool IsMessageInTrash(MessageModel message)
        {
            mailBoxHomePage.TrashButton.Click();
            Thread.Sleep(1000);
            mailBoxHomePage.CheckMailButton.Click();
            Thread.Sleep(1000);

            return mailBoxHomePage.IsMessageExists(message.MessageSubject);
        }

        public void DeleteMessageToTrash(MessageModel message)
        {
            if (!IsMessageInInbox(message)) return;

            mailBoxHomePage.MessageCheckBoxButton(message.MessageSubject).Click();
            Thread.Sleep(1000);
            mailBoxHomePage.DeleteButton.Click();
        }

        public void DeleteMessageFromTrash(MessageModel message)
        {            
            if (!IsMessageInTrash(message)) return;

            mailBoxHomePage.MessageCheckBoxButton(message.MessageSubject).Click();
            Thread.Sleep(1000);
            mailBoxHomePage.DeleteButton.Click();
        }
    }
}
