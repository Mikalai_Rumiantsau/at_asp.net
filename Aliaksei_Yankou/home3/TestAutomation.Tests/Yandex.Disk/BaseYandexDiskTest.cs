﻿using System.Configuration;
using TestAutomation.Tests;

namespace TestAutomation.Yandex.Disk.Tests
{
	public class BaseYandexDiskTest : BaseTest
	{
		public BaseYandexDiskTest()
		{
			this.baseUrl = ConfigurationManager.AppSettings["YandexDiskUrl"];
		}
	}
}
