﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAutomation.Framework;

namespace TestAutomation.Pages
{
	internal static class MailLoginPage
	{
		private static readonly By loginInputByCss = By.CssSelector("input[name=login]");
		private static readonly By passwordInputByCss = By.CssSelector("input[name=passwd]");
		private static readonly By submitButtonByCss = By.CssSelector(".domik2__submit button[type='submit']");

		internal static IWebElement LoginInput { get { return Browser.Instance.FindElement(loginInputByCss); } }
		internal static IWebElement PasswordInput { get { return Browser.Instance.FindElement(passwordInputByCss); } }
		internal static IWebElement SubmitButton { get { return Browser.Instance.FindElement(submitButtonByCss); } }
	}
}
