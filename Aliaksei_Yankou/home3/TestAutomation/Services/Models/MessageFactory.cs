﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomation.Services.Models
{
	public static class MessageFactory
	{
		private const string defaultTo = "yankoua@yandex.com";
		private const string defaultSubject = "test subject ";
		private const string defaultText = "test message body ";

		private static readonly string invalidTo = new Random().Next().ToString();
		private static readonly string subject = defaultSubject
			+ DateTime.Now.ToShortDateString() + " "
			+ DateTime.Now.ToLongTimeString();
		private static readonly string body = defaultText 
			+ DateTime.Now.ToShortDateString() + " "
			+ DateTime.Now.ToLongTimeString();

		public static Message ValidRecipientMessage
		{
			get { return new Message(defaultTo, subject, body); }
		}

		public static Message InvalidRecipientMessage
		{
			get { return new Message(invalidTo, subject, body); }
		}

	}
}
