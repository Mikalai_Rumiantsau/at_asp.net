﻿using AutomationDemo.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class MessageMailRemoveInBasketPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By messageMailRemoveInBasketByCss = By.CssSelector("....");//сообщение об удалении в корзину

        internal IWebElement MessageMailRemoveInBasket()
        {
            return browser.FindElement(messageMailRemoveInBasketByCss); 
        } 
    }
}
