﻿using AutomationDemo.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class MessageMailDeletePermanentFromBasketPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By messageMailDeletePermanentFromBasketPageByCss = By.CssSelector("....");//сообщение об удалении из корзины

        internal IWebElement MessageMailDeletePermanentFromBasket()
        {
            return browser.FindElement(messageMailDeletePermanentFromBasketPageByCss); 
        } 
    }
}
