﻿using AutomationDemo.Framework;
using AutomationDemo.Services.Models;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class IncomingLettersPage
    {
        private Browser browser = Browser.Instance;
        private static string selectorCSSLetterFromLetterFactory = String.Format("span[title='{0}']", LetterFactory.defaultTheme);
        private static string selectorXPathCheckBoxLetter = String.Format(".//span[@title='{0}']/ancestor :: div[@class='mail-MessageSnippet-Wrapper']//input[@class = '_nb-checkbox-input']", LetterFactory.defaultTheme);

        private static readonly By letterFromLetterFactoryByCss = By.CssSelector(selectorCSSLetterFromLetterFactory);
        private static readonly By checkBoxLetterByXPath = By.XPath(selectorXPathCheckBoxLetter);
        private static readonly By buttonRemoveInABasketButtonByCss = By.CssSelector("div[title='Удалить (Delete)']");
        private static readonly By buttonOutgoingLettersButtonByCss = MailBoxPage.buttonOutgoingLettersByCss;

        public IWebElement FindLetterFromLetterFactory ()
        {
            return browser.FindElement(letterFromLetterFactoryByCss); 
        }
        public IWebElement FindCheckBoxLetter() 
        {
            return browser.FindElement(checkBoxLetterByXPath); 
        }
        public IWebElement FindButtonRemoveInABasketButton() 
        {
            return browser.FindElement(buttonRemoveInABasketButtonByCss); 
        }

        public IWebElement FindButtonOutgoingLettersButton()
        {
            return browser.FindElement(buttonOutgoingLettersButtonByCss);
        }
    }
}
