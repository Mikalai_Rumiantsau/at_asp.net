﻿using AutomationDemo.Framework;
using AutomationDemo.Services.Models;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class MailBoxPage
    {
        private Browser browser = Browser.Instance;
        
        public static string anchorInMailBoxSelector = String.Format(".//*[@class='mail-User-Name'][text()='{0}']",AccountFactory.defaultUserEmail);

        public static readonly By anchorInMailBoxByXPath = By.XPath(anchorInMailBoxSelector);//отличительный узел на странице почты
        public static readonly By buttonNewLetterByCss = By.CssSelector("a[title='Написать (w, c)']");//селектор кнопки "написать письмо"
        public static readonly By buttonIncomingLettersByCss = By.CssSelector("a[title~='Входящие']");//селектор ссылки/кнопки "входящие письма"
        public static readonly By buttonOutgoingLettersByCss = By.CssSelector("a[title~='Отправленные']");//селектор ссылки/кнопки "исходящие письмо"
        public static readonly By buttonBasketByCss = By.CssSelector("a[title~='Удалённые']");//селектор ссылки/кнопки "корзина"

        public IWebElement SelectAnchorInMailBox()
        {
            return browser.FindElement(anchorInMailBoxByXPath);
        }

        public IWebElement SelectButtonNewLetter()
        {
            return browser.FindElement(buttonNewLetterByCss);
        }

        public IWebElement SelectButtonIncomingLetters()
        {
            return browser.FindElement(buttonIncomingLettersByCss);
        }

        public IWebElement SelectButtonOutgoingLetters()
        {
            return browser.FindElement(buttonOutgoingLettersByCss);
        }

        public IWebElement SelectButtonBasket()
        {
            return browser.FindElement(buttonBasketByCss);
        }
    }
}
