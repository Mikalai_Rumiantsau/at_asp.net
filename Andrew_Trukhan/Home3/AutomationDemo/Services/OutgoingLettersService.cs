﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;
using AutomationDemo.Pages;

namespace AutomationDemo.Services
{
    public class OutgoingLettersService
    {
        private OutgoingLettersPage outgoingLettersPage = new OutgoingLettersPage();

        public bool IsLetterFromLetterFactory ()
        {
            return (outgoingLettersPage.FindLetterFromLetterFactory() != null);
        }
    }
}
