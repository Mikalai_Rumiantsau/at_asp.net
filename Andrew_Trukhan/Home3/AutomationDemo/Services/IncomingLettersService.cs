﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;
using AutomationDemo.Pages;

namespace AutomationDemo.Services
{
    public class IncomingLettersService
    {
        private IncomingLettersPage incomingLettersPage = new IncomingLettersPage();

        public bool IsLetterFromLetterFactory ()
        {
            return (incomingLettersPage.FindLetterFromLetterFactory() != null);
        }

        public void RemoveInABasket() //удаление в корзину
        {
            NoteCheckBoxLetter();
            incomingLettersPage.FindButtonRemoveInABasketButton().Click();
        }

        private void NoteCheckBoxLetter()
        {
            incomingLettersPage.FindCheckBoxLetter().Click(); 
        }

        public void ClickButtonOutgoingLetters() //переход в папку "отправленные"
        {
            incomingLettersPage.FindButtonOutgoingLettersButton().Click();
        }

    }
}
