﻿using NUnit.Framework;
using AutomationDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;

namespace AutomationDemo.Tests
{
    [TestFixture]
    class RemoveLetterInBasketTest: BaseTest
    {
        LoginService loginService = new LoginService();
        MailBoxService mailBoxService = new MailBoxService();
        CreateAndSendLetterService createAndSendLetterService = new CreateAndSendLetterService();
        BasketPageService basketPageService = new BasketPageService();
        IncomingLettersService incomingLettersService = new IncomingLettersService();

        [Test]//тест проверки удаления в корзину
        public void RemoveLetterInBasketTest_Appeared_True_FromMethod_IsRemovedMailInBasket()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailBoxService.ClickButtonNewLetter();
            createAndSendLetterService.CreateAndSendLetter(LetterFactory.CreateCorrectLetter());
            mailBoxService.ClickButtonIncomingLetters();
            incomingLettersService.RemoveInABasket();
            mailBoxService.ClickButtonBasket();
            bool value = incomingLettersService.IsLetterFromLetterFactory();
            Assert.That(value, Is.EqualTo(false));
        }
    }


}
