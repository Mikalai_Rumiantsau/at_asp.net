﻿using NUnit.Framework;
using WorkForAutomation.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkForAutomation.Test
{
    class BaseTest
    {
        private const string baseUrl = "http://www.yandex.by";

        [SetUp]
        public void StartBrowser()
        {
            Browser.Instance.Start().OpenAt(baseUrl);
        }

        [TearDown]
        public void CloseBrowser()
        {
            Browser.Instance.Close();
        }
    }
}
