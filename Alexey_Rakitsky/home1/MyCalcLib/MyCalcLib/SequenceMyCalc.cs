﻿
namespace MyCalcLib
{
    public class SequenceMyCalc
    {
        private ISequence sequence;

        public SequenceMyCalc(ISequence sequence)
        {
            this.sequence = sequence;
        }

        public int NextSum()
        {
            int sum = 0;
            sequence.NextInts().ForEach(x => sum += x);
            return sum;
        }
    }
}
