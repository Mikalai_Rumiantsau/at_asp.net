﻿using AutomationYandexMail.Framework;
using OpenQA.Selenium;

namespace AutomationYandexMail.Pages
{
    class NewMessagePage
    {
        private Browser browser = Browser.Instance;

        private static readonly By SendEmailByCss = By.CssSelector("div[name='to']");
        private static readonly By MessageThemeByCss = By.CssSelector("input[name='subj']");
        private static readonly By MessageTextByCss = By.CssSelector("div[role = 'textbox']");
        private static readonly By SendButtonByXPath = By.XPath("(//button[contains(@class,'js-send')])[1]");
        
        internal IWebElement SendEmail { get { return browser.FindElement(SendEmailByCss); } }
        internal IWebElement MessageTheme { get { return browser.FindElement(MessageThemeByCss); } }
        internal IWebElement MessageText { get { return browser.FindElement(MessageTextByCss); } }
        internal IWebElement SendButton { get { return browser.FindElement(SendButtonByXPath); } }

    }
}
