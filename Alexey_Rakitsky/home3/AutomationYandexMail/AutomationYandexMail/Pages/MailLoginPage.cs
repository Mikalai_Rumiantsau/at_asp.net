﻿using AutomationYandexMail.Framework;
using OpenQA.Selenium;

namespace AutomationYandexMail.Pages
{
    class MailLoginPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By loginInputByCss = By.CssSelector("input[name=login]");
        private static readonly By passwordInputByCss = By.CssSelector("input[name=passwd]");
        private static readonly By submitButtonByCss = By.CssSelector(".domik2__submit button[type='submit']");
        
        public IWebElement LoginInput { get { return browser.FindElement(loginInputByCss); } }
        public IWebElement PasswordInput { get { return browser.FindElement(passwordInputByCss); } }
        public IWebElement SubmitButton { get { return browser.FindElement(submitButtonByCss); } }
    }
}
