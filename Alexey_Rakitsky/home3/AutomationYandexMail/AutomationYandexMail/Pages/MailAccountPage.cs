﻿using AutomationYandexMail.Framework;
using OpenQA.Selenium;
using System.Collections.ObjectModel;

namespace AutomationYandexMail.Pages
{
    class MailAccountPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By InboxButtonByCss = By.CssSelector("div > [href='#inbox']");
        private static readonly By SentButtonByCss = By.CssSelector("div > [href='#sent']");
        private static readonly By TrashButtonByCss = By.CssSelector("div > [href='#trash']");
        private static readonly By CheckButtonByCss = By.CssSelector("div[data-click-action='mailbox.check']");
        private static readonly By ComposeButtonByCss = By.CssSelector("div.ns-view-container-desc > a[href*='#compose']");
        private static readonly By DeleteButtonByCss = By.CssSelector("div.mail-Toolbar-Item_delete");
        private static readonly By MessageCheckBoxByCss = By.CssSelector("label[data-nb='checkbox']");

        internal IWebElement Message(string messageTitle)
        {
            return browser.FindElement(By.XPath("//span[@title='" + messageTitle + "']/ancestor::a"));
        }
        internal IWebElement GetMarkMessageCheckbox(string messageTitle)
        {
            return Message(messageTitle).FindElement(MessageCheckBoxByCss);
        }
        internal IWebElement DeleteButton { get { return browser.FindElement(DeleteButtonByCss); } }
        internal IWebElement ComposeButton { get { return browser.FindElement(ComposeButtonByCss); } }
        internal IWebElement CheckButton { get { return browser.FindElement(CheckButtonByCss); } }
        internal IWebElement InboxButton { get { return browser.FindElement(InboxButtonByCss); } }
        internal IWebElement SentButton { get { return browser.FindElement(SentButtonByCss); } }
        internal IWebElement TrashButton { get { return browser.FindElement(TrashButtonByCss); } }
    }
}