﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using AutomationDemo.Services;
using AutomationDemo.Services.Models;


namespace AutomationDemo.Tests
{
     [TestFixture]
    class SendingLetterTest : BaseTest
    {
         LoginService loginService = new LoginService();
         OptionMessageService optionMessageService = new OptionMessageService();


         [Test]
         public void SendingLetter_Test()
         {
             loginService.LoginToMailBox(AccountFactory.Account);
             optionMessageService.SendNewMessage_andReturnInbox();
             bool flag = true;
             bool findLetter = optionMessageService.UniqueLetterFind();
             if (findLetter)
             {
                 optionMessageService.OpenSentMessage();
                 bool findLetterInSent = optionMessageService.UniqueLetterFind();
                 if (!findLetterInSent)
                 {
                     flag = false;
                 }

             }
             else
             {
                 flag = false;
             }
             Assert.IsTrue(flag);
         }
    }
}
