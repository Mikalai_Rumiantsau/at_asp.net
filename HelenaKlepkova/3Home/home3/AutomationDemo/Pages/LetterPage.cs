﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Framework;
using OpenQA.Selenium;
using AutomationDemo.Services;

namespace AutomationDemo.Pages
{
    class LetterPage
    {
        Browser browser = Browser.Instance;
        static readonly By DeleteLetterButtonByCss = By.CssSelector("div[class*='button-delete']");

        internal IWebElement DeleteLetterButton { get { return browser.FindElement(DeleteLetterButtonByCss); } }

    }
}
