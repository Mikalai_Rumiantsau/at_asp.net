﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Unit
{
    public class Calc
    {
        public int Sum(int a, int b)
        {
            return a + b;
        }

        public int Substract(int a, int b)
        {
            // Mistake added for test to be fail
            return a - b - 1;
        }

        public double Divide(double a, double b)
        {
            if (b == 0)
                throw new ArgumentException("Divider should be not 0.");
            return a / b;
        }

        public int Multiple(int a, int b)
        {
            return a * b;
        }
    }
}
