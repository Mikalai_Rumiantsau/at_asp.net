﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;

namespace AutoTests.Utilities
{

    public static class Browser
    {

        private static IWebDriver webDriver;

        public static IWebDriver WebDriver
        {
            get { return webDriver ?? StartWebDriver(); }
        }


        private static IWebDriver StartWebDriver()
        {

            Contract.Ensures(Contract.Result<IWebDriver>() != null);

            if (webDriver != null) return webDriver;
            webDriver = StartChrome();
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            webDriver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(20));
            webDriver.Manage().Window.Maximize();
            return WebDriver;
        }

        private static ChromeDriver StartChrome()
        {
            return new ChromeDriver();
        }


        public static void Start()
        {
            webDriver = StartWebDriver();
        }


        public static void Navigate(string url)
        {
            Contract.Requires(url != null);

            WebDriver.Navigate().GoToUrl(url);
        }


        public static void Quit()
        {
            if (webDriver == null) return;
            webDriver.Quit();
            webDriver = null;
            /*
            Process[] chromeDriverProcesses = Process.GetProcessesByName("chromedriver");

            foreach (var chromeDriverProcess in chromeDriverProcesses)
            {
                chromeDriverProcess.Kill();
            }
           */ 
        }

        public static IWebElement FindElement(By selector)
        {
            Contract.Assume(WebDriver != null);
            try
            {
                return WebDriver.FindElement(selector);
            }
            catch (NoSuchElementException ex)
            {
                throw ex;
            }

        }

    }
}
