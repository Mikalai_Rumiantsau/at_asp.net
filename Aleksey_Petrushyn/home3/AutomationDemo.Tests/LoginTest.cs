﻿using NUnit.Framework;
using AutomationDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;

namespace AutomationDemo.Tests
{
	[TestFixture]
	class LoginTest : BaseTest
	{
		LoginService loginService = new LoginService();

		private const string expected_error_message_in_case_of_account_not_exist = "Нет аккаунта с таким логином.";
		private const string expected_error_message_in_case_of_wrong_password = "Неправильный логин или пароль.";

		[Test]
		public void LoginTest_LoginWithNonExistingAccount_CorrectErrorMessagePresent()
		{
			loginService.LoginToMailBox(AccountFactory.NonExistedAccount);
			string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
			Assert.That(expected_error_message_in_case_of_account_not_exist, Is.EqualTo(actualErrorMessage));
		}

		[Test]
		public void LoginTest_LoginWithAccountWithWrongPaswd_CorrectErrorMessagePresent()
		{
			loginService.LoginToMailBox(AccountFactory.AccountWithWrongPassword);
			string actualErrorMessage = loginService.RetrieveErrorOnFailedLogin();
			Assert.That(expected_error_message_in_case_of_wrong_password, Is.EqualTo(actualErrorMessage));
		}
		[Test]
		public void LoginTest_AuthorizationSuccessful()   //positive test - search button "Write"
		{
			loginService.LoginToMailBox(AccountFactory.Account);
			bool isDisplayedSendButton = loginService.IsDisplayedWriteButton();
			Assert.That(isDisplayedSendButton);
		}
	}
}
